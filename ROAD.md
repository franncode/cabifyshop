# ROAD TO SUCCESS

## Intro

Trate de utilizar los tiempos de manera sabia, no incluir demasiadas cosas, haciendo foco en la calidad de lo entregado.

## Tech Stack

Leyendo [este articulo](https://cabify.tech/engineering/our-frontend-stack/) publicado por desarrolladores de Cabify, tome tecnologias que utilice en este proyecto y con las cuales me siento muy comodo, estas son:

- React
- Typescript
- Context API
- SASS
- Internationalization
- Jest
- React Testing Library
- ESLint
- Stylelint
- Prettier
- Gitlab
- Renovatebot
- Dependabot
- Rollbar

Por cierto, muy interesante breve articulo sobre el camino que se recorrio para llegar al stack actual, ademas como tecnologia core decidi usar Next.JS para un desarrollo mas rapido ya que trae todo preconfigurado y es una tecnologia con la que disfruto desarrollar.

## Asumimos

Que traigo el cart que tengo guardado en el backend

## Checkout Class y el antipatron en manejo de estados en React

No es una buena opcion usar una instancia de la clase Checkout como estado en React, ya que para agregar productos a este estado debemos realizarlo a traves del metodo `scan` de esta clase y no a traves de la funcion que devuelve el hook `useState` ademas de no ser recomendado no dispara un nuevo render, ya que la consiga del ejercicio asi lo pide de todas formas, encontre como workaround pasarle una instancia clonada con el producto agregado a `setState` para que reconozca este cambio y sitúa en la cola una nueva renderización del componente.
Para crear esta solucion fue util hacerlo con TDD, donde primero se escribio el test y luego el desarrollo, ya que contaba con la informacion de como debia instanciarse y usarse esta clase.
CITAR AL EJEMPLO DEL CHALLENGE.
Es igual de malo o peor mutar el store y no hacerlo a traves de dispach/action, esto iria en contra de la programacion funcional.

## Components structure

Para estructurar los componentes utilice la metodologia propuesta por Atomic Design
https://bradfrost.com/blog/post/atomic-web-design/

## Tipos

Utilice `interfaces` para entidades que existen en esta aplicacion y `types` para funciones, metodos y componentes

## Para revisar

- https://nextjs.org/docs/testing
- https://rollbar.com/
- https://docs.rollbar.com/docs/react-ts
- https://docs.rollbar.com/docs/javascript

## To Do

- Crear componentes
- Crear theme scss
- Redux
- Test unitario
- HACER UNA VERSION LIMPIA EN DEVELOP, AL TERMINAR, PASARLA A LA RAMA feature/clean-code
- EN DEVELOP HACERLO HORRIBLE PARA PODER USAR LA CLASE QUE PIDE EL EJERCICIO

## ERRORES

Planifique mover a componentes el markup y estilos a componentes, no salio bien, por lo que decidi crear mis propios estilos y markupxx
empece con redux pero lo tuve que quitar, tengo dos ramas con versiones.
