import { IItem } from "."

export interface IDiscount {
  code: string
  item: Pick<IItem, "code">
  name: string
  value: number
}
