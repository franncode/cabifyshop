import { IDiscount, IItemCart } from "."

export interface IPricingRule extends Omit<IDiscount, "value"> {
  apply: string
  rule: string
}
