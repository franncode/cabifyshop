export interface IItem {
  code: string
  image?: {
    alt: string
    src: string
  }
  name: string
  price: number
}
