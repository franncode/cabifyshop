import { IItem } from "./item"

export interface IItemCart extends IItem {
  quantity: number
  total: number
}
