import { IItem } from "../../interfaces"
import { rollbarInstance } from "../../settings/rollbar"
import data from "./items.json"

const getAll = async (): Promise<IItem[]> => {
  try {
    return Promise.resolve(data)
  } catch (error) {
    rollbarInstance.error("Error while fetching items", {
      error,
      method: "items.getAll",
    })
    return []
  }
}

export const items = {
  getAll,
}
