import { IPricingRule } from "../../interfaces"
import { rollbarInstance } from "../../settings/rollbar"
import data from "./pricingRules.json"

const getAll = async (): Promise<IPricingRule[]> => {
  try {
    return Promise.resolve(data)
  } catch (error) {
    rollbarInstance.error("Error while fetching pricing rules", {
      error,
      method: "pricingRules.getAll",
    })
    return []
  }
}

export const pricingRules = {
  getAll,
}
