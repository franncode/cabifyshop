import { IItemCart } from "../../interfaces"
import { rollbarInstance } from "../../settings/rollbar"
import data from "./cart.json"

const get = async (): Promise<Pick<IItemCart, "code" | "quantity">[]> => {
  try {
    return Promise.resolve(data)
  } catch (error) {
    rollbarInstance.error("Error while fetching items", {
      error,
      method: "items.getAll",
    })
    return []
  }
}

export const cart = {
  get,
}
