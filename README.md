# CabifyShop by franncode

## Intro

I hope you like my solution, try to do my best when developing the requested features and those mentioned as bonus 🚀.

## Tech Stack

When reading [this article](https://cabify.tech/engineering/our-frontend-stack/) published by Cabify developers, I took some technologies that they use and with which I feel very comfortable, these are:

1. Version control

   - [Conventional commits](https://www.conventionalcommits.org/en/v1.0.0/)
   - Git
   - Gitflow
   - Gitlab

2. UI

   - React
   - SASS

3. Quality

   - Dependabot
   - Jest
   - Prettier
   - React Testing Library
   - Renovatebot
   - Typescript
   - Stylelint
   - ESLint
   - Rollbar

4. State management
   - Context API
   - Redux (Rama WIP: [feature/with-redux](https://gitlab.com/cabify-challenge/frontend-shopping-cart-challenge-franncode/-/tree/feature/with-redux))

Also as core technology I decided to use Next.JS for a faster development since it comes with everything preconfigured and it is a technology that I enjoy developing with.
BTW very interesting article by the Cabify team 👆 about the path that the Cabify team traveled to reach the current stack.

## Assumptions

- The first render will show a cart with products and their quantities as seen in the index.html file of the challenge, simulating showing a shopping cart obtained from our backend.
- To create the unit tests of the Checkout class, I use the information provided in the original README.md file as a basis.

## Checkout Class and the anti-pattern in handling states in React

IMHO, It is not a good option to use an instance of the Checkout class as a state in React, since to add products to this state we must do it through the `scan` method of this class and not through the function that returns the `useState` hook since not doing it through this function does not trigger a new render, since it is a requirement of the exercise, I was able to do a hack so that every time you want to mutate the checkout, after this mutation, obtain information about the class, such as its items and finally generate a new data in memory to make useState believe that this state had changed and then place a new rendering of the component in the queue.
When wanting to carry out this implementation with Redux, I also found myself with a challenge since the way of changing the information through the checkout class and its methods go against the pattern (Flux)[https://facebook.github.io/flux/docs/in-depth-overview/].

## Components structure

To structure the components, I use the methodology proposed by (Atomic Design)[https://bradfrost.com/blog/post/atomic-web-design/] and only perform snapshot tests of the atomic components.

## Types

I use `interfaces` for entities that exist in this application and `types` for functions, methods and components.
In this solution I think there are three main entities, `Discounts`, `Cart` and `Checkout`.

# Next.js

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.tsx`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.ts`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
