import { FC } from "react"
import { useCheckout } from "../../../hooks/useCheckout/useCheckout"
import { currency } from "../../../features/currency/currency"
import { IItemCart } from "../../../interfaces"
import { Button } from "../../atoms/button/button"
import { Line } from "../../atoms/line/line"
import { Close } from "../../icons/close"
import { Frame } from "../../organisms/frame/frame"
import styles from "./itemDetails.module.scss"

export const ItemDetails: FC<{ item: IItemCart; onClose: () => void }> = ({
  item,
  onClose,
}) => {
  const { addItem } = useCheckout()

  const handleAdd = () => {
    addItem(item.code)
    onClose()
  }

  return (
    <Frame className={styles.itemdetails}>
      <img
        alt={item.name}
        className={styles.image}
        src={`/products/${item.name.toLowerCase()}.jpg`}
      />
      <aside className={styles.aside}>
        <Close className={styles.close} onClick={onClose} />
        <span className={styles.title}>
          <h2 className={styles.text}>{item.name}</h2>
          <h2 className={styles.text}>{currency.toEUR(item.price)}</h2>
        </span>
        <Line />
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sodales
          semper elit sit amet interdum. Praesent volutpat sed elit vel
          consectetur. Nulla tempus tincidunt ex, sit amet semper ipsum
          imperdiet varius. In rutrum aliquam nisl, sagittis faucibus felis
          bibendum id.
        </p>
        <Line />
        <p>Product code {item.code}</p>
        <Button fill onClick={handleAdd}>
          Add to cart
        </Button>
      </aside>
    </Frame>
  )
}
