import { IItemCart } from "../../../interfaces"

export type CartDetailsProps = {
  onPreview: (item: IItemCart) => void
}
