import { FC } from "react"
import { useCheckout } from "../../../hooks/useCheckout/useCheckout"
import { Button } from "../../atoms/button/button"
import { Line } from "../../atoms/line/line"
import { ItemTable } from "../../molecules/itemTable/itemTable"
import { Summary } from "../../molecules/summary/summary"
import { Frame } from "../../organisms/frame/frame"
import styles from "./cartDetails.module.scss"
import { CartDetailsProps } from "./cartDetails.types"

export const CartDetails: FC<CartDetailsProps> = ({ onPreview }) => {
  const { discounts, items, addItem, removeItem, total } = useCheckout()

  const itemsQuantity = items.reduce(
    (total, { quantity }) => total + quantity,
    0
  )
  const discountsSummary = discounts.map(({ name, value }) => ({
    name,
    total: value,
  }))
  const check = [
    {
      items: [
        {
          name: `${itemsQuantity} items`,
          total: total.gross,
        },
      ],
    },
    {
      title: "DISCOUNTS",
      items: discountsSummary,
    },
  ]
  const final = {
    name: "TOTAL COST",
    total: total.net,
  }

  return (
    <Frame className={styles.checkout}>
      <section className={styles.cart}>
        <h2 className={styles.title}>Shopping cart</h2>
        <Line />
        <ItemTable
          items={items}
          onAdd={addItem}
          onChange={addItem}
          onPreview={onPreview}
          onRemove={removeItem}
        />
      </section>
      <aside className={styles.summary}>
        <h2 className={styles.title}>Order Summary</h2>
        <Line />
        <Summary summaries={check} final={final} />
        <Button fill>Checkout</Button>
      </aside>
    </Frame>
  )
}
