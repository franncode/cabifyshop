type SummaryItem = {
  name: string
  total: number
}

type Summary = {
  items: SummaryItem[]
  title?: string
}

export type SummaryProps = {
  final: SummaryItem
  summaries: Summary[]
}
