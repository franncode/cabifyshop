import { FC, Fragment } from "react"
import { currency } from "../../../features/currency/currency"
import { Line } from "../../atoms/line/line"
import styles from "./summary.module.scss"
import { SummaryProps } from "./summary.types"

export const Summary: FC<SummaryProps> = ({ final, summaries }) => (
  <>
    {summaries.map(({ title, items }, index) => (
      <Fragment key={index}>
        <div className={styles.item}>
          {title && <p className={styles.title}>{title}</p>}
          <ul className={styles.descriptions}>
            {items.map(({ name, total: _total }) => {
              const total = currency.toEUR(_total)

              return (
                <li key={name} className={styles.description}>
                  <p className={styles.name}>{name}</p>
                  <p className={styles.total}>{total}</p>
                </li>
              )
            })}
          </ul>
        </div>
        <Line />
      </Fragment>
    ))}
    <span className={styles.final}>
      <p className={styles.label}>{final.name}</p>
      <p className={styles.net}>{currency.toEUR(final.total)}</p>
    </span>
  </>
)
