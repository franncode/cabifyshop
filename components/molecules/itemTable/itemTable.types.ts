import { IItemCart } from "../../../interfaces"
import { ItemRowProps } from "../../atoms/itemRow/itemRow.types"

export type ItemTableProps = Pick<
  ItemRowProps,
  "onAdd" | "onChange" | "onPreview" | "onRemove"
> & {
  items: IItemCart[]
}
