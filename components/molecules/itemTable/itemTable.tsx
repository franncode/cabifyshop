import { FC } from "react"
import { ItemRow } from "../../atoms/itemRow/itemRow"
import { ItemTableProps } from "./itemTable.types"
import styles from "./itemTable.module.scss"

export const ItemTable: FC<ItemTableProps> = ({
  items,
  onAdd,
  onChange,
  onPreview,
  onRemove,
}) => {
  const headers = ["PRODUCT DETAILS", "QUANTITY", "PRICE", "TOTAL"]

  return (
    <div className={styles.itemtable}>
      <ul className={styles.head}>
        {headers.map((header) => (
          <li className={styles.header} key={header}>
            {header}
          </li>
        ))}
      </ul>
      <ul className={styles.body}>
        {items.map((item) => (
          <ItemRow
            item={item}
            key={item.code}
            onAdd={onAdd}
            onChange={onChange}
            onPreview={onPreview}
            onRemove={onRemove}
          />
        ))}
      </ul>
    </div>
  )
}
