import { FC } from "react"
import { ButtonProps } from "./button.types"
import styles from "./button.module.scss"

export const Button: FC<ButtonProps> = ({
  children,
  fill = false,
  variant = "primary",
  ...props
}) => (
  <button
    className={`
    ${styles.button}
    ${fill ? styles.button_fill : ""}
    ${styles[`button_${variant}`]}
    `}
    {...props}
  >
    {children}
  </button>
)
