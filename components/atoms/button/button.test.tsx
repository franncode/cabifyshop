import renderer from "react-test-renderer"
import { Button } from "./button"

describe("Component: Button", () => {
  test("Renders correctly", () => {
    const tree = renderer
      .create(<Button onClick={jest.fn()}>Click me</Button>)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})
