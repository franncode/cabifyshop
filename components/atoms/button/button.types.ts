import { ButtonHTMLAttributes, DetailedHTMLProps, ReactNode } from "react"

export type ButtonProps = DetailedHTMLProps<
  ButtonHTMLAttributes<HTMLButtonElement>,
  HTMLButtonElement
> & {
  children: ReactNode
  fill?: boolean
  variant?: "primary" | "secondary"
}
