import { FC } from "react"
import styles from "./line.module.scss"

export const Line: FC = () => <hr className={styles.line} />
