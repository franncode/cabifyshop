import renderer from "react-test-renderer"
import { Line } from "./line"

describe("Component: Line", () => {
  test("Renders correctly", () => {
    const tree = renderer.create(<Line />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
