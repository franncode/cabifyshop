import renderer from "react-test-renderer"
import { ItemRow } from "./itemRow"

describe("Component: ItemRow", () => {
  test("Renders correctly", () => {
    const tree = renderer
      .create(
        <ItemRow
          item={{
            code: "X7R2OPX",
            image: {
              alt: "t-shirt with initials C O on the chest",
              src: "/products/shirt.png",
            },
            name: "Shirt",
            price: 20,
            quantity: 3,
            total: 60,
          }}
          onAdd={jest.fn()}
          onChange={jest.fn()}
          onPreview={jest.fn()}
          onRemove={jest.fn()}
        />
      )
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})
