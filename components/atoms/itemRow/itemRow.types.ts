import { IItem, IItemCart } from "../../../interfaces"

export type ItemRowProps = {
  className?: string
  item: IItemCart
  onAdd: (code: IItem["code"]) => void
  onChange: (code: IItemCart["code"], quantity: IItemCart["quantity"]) => void
  onPreview: (item: IItemCart) => void
  onRemove: (code: IItemCart["code"]) => void
}
