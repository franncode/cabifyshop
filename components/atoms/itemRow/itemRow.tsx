import { FC } from "react"
import { currency } from "../../../features/currency/currency"
import { Aggregator } from "../aggregator/aggregator"
import styles from "./itemRow.module.scss"
import { ItemRowProps } from "./itemRow.types"

export const ItemRow: FC<ItemRowProps> = ({
  className,
  item,
  onAdd,
  onChange,
  onPreview,
  onRemove,
}) => {
  const { code, image, name, price: _price, quantity, total: _total } = item
  const price = currency.toEUR(_price)
  const total = currency.toEUR(_total)

  return (
    <li className={`${styles.itemrow} ${className || ""}`}>
      <div className={styles.details} onClick={() => onPreview(item)}>
        <img
          alt={image?.alt || "item without image"}
          className={styles.image}
          src={image?.src || "/products/notFound.png"}
        />
        <div className={styles.text}>
          <p className={styles.name}>{name}</p>
          <p className={styles.code}>Product code {code}</p>
        </div>
      </div>
      <Aggregator
        value={quantity}
        onAdd={() => onAdd(item.code)}
        onChange={({ target }) => onChange(item.code, Number(target.value))}
        onRemove={() => onRemove(code)}
      />
      <p className={styles.price}>{price}</p>
      <p className={styles.total}>{total}</p>
    </li>
  )
}
