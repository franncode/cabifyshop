import { FC } from "react"
import NextHead from "next/head"
import { HeadProps } from "./head.types"

export const Head: FC<HeadProps> = ({ title = "CabifyShop" }) => (
  <NextHead>
    <link rel="apple-touch-icon" href="/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" href="/android-icon-192x192.png" />
    <link rel="manifest" href="/manifest.json" />
    <link rel="shortcut icon" type="image/png" href="/favicon-32.png" />
    <meta charSet="utf-8" />
    <meta name="theme-color" content="#7350FF" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>{title}</title>
  </NextHead>
)
