import renderer from "react-test-renderer"
import { Head } from "./head"

describe("Component: Head", () => {
  test("Renders correctly", () => {
    const tree = renderer.create(<Head title="Cabify" />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
