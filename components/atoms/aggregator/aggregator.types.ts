import { ChangeEventHandler } from "react"

export type AggregatorProps = {
  onAdd: () => void
  onChange: ChangeEventHandler<HTMLInputElement>
  onRemove: () => void
  value: number
}
