import renderer from "react-test-renderer"
import { Aggregator } from "./aggregator"

describe("Component: Aggregator", () => {
  test("Renders correctly", () => {
    const tree = renderer
      .create(
        <Aggregator
          onAdd={jest.fn()}
          onChange={jest.fn()}
          onRemove={jest.fn()}
          value={0}
        />
      )
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})
