import { FC, KeyboardEvent } from "react"
import { AggregatorProps } from "./aggregator.types"
import styles from "./aggregator.module.scss"

export const Aggregator: FC<AggregatorProps> = ({
  onAdd,
  onChange,
  onRemove,
  value,
}) => {
  const handleKeyDown = (event: KeyboardEvent<HTMLInputElement>) => {
    if (
      event.key === "." ||
      event.key === "-" ||
      event.key === "e" ||
      (event.key === "ArrowDown" && value === 0)
    ) {
      event.preventDefault()
      event.stopPropagation()
    }
  }

  return (
    <span className={styles.aggregator}>
      <button className={styles.button} onClick={onRemove} type="button">
        -
      </button>
      <input
        aria-label="Product quantity"
        className={styles.input}
        onChange={onChange}
        type="number"
        value={value || ""}
        onKeyDown={handleKeyDown}
      />
      <button className={styles.button} onClick={onAdd} type="button">
        +
      </button>
    </span>
  )
}
