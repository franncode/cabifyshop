import { ReactNode } from "react"
import { HeadProps } from "../../atoms/head/head.types"

export type FrameProps = {
  children: ReactNode
  className?: string
  head?: HeadProps
}
