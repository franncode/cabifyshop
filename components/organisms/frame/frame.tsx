import { FC } from "react"
import { Head } from "../../atoms/head/head"
import { FrameProps } from "./frame.types"
import styles from "./frame.module.scss"

export const Frame: FC<FrameProps> = ({ children, className, head }) => (
  <>
    <Head {...head} />
    <main className={`${styles.frame} ${className}`}>{children}</main>
  </>
)
