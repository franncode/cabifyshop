import { createContext, FC, ReactNode, useContext, useState } from "react"
import { IDiscount, IItemCart } from "../../interfaces"
import { Checkout } from "../../features/checkout/checkout"
import { CheckoutContextType } from "./useCheckout.types"

import serverCart from "../../api/cart/cart.json"
import availableItems from "../../api/items/items.json"
import pricingRules from "../../api/pricingRules/pricingRules.json"
const checkout = new Checkout(availableItems, pricingRules, serverCart)

const initialCheckoutContext: CheckoutContextType = {
  addItem: (code, quantity) => undefined,
  discounts: [],
  items: [],
  removeItem: (code) => undefined,
  total: {
    gross: 0,
    net: 0,
  },
}

const CheckoutContext = createContext(initialCheckoutContext)

export const CheckoutProvider: FC<{ children: ReactNode }> = ({ children }) => {
  const [items, setItems] = useState<IItemCart[]>(checkout.getItems())
  const [discounts, setDiscounts] = useState<IDiscount[]>(
    checkout.getDiscounts()
  )
  const [total, setTotal] = useState<{ gross: number; net: number }>({
    gross: checkout.totalWithoutDiscounts(),
    net: checkout.total(),
  })

  const addItem = (code: IItemCart["code"], quantity?: number) => {
    const nextCheckout = checkout.scan(code, quantity)
    const nextTotal = {
      gross: nextCheckout.totalWithoutDiscounts(),
      net: nextCheckout.total(),
    }

    setItems([...nextCheckout.getItems()])
    setDiscounts([...nextCheckout.getDiscounts()])
    setTotal({ ...nextTotal })
  }

  const removeItem = (code: IItemCart["code"]) => {
    const nextCheckout = checkout.unscan(code)
    const nextTotal = {
      gross: nextCheckout.totalWithoutDiscounts(),
      net: nextCheckout.total(),
    }

    setItems([...nextCheckout.getItems()])
    setDiscounts([...nextCheckout.getDiscounts()])
    setTotal({ ...nextTotal })
  }

  return (
    <CheckoutContext.Provider
      value={{ addItem, discounts, items, removeItem, total }}
    >
      {children}
    </CheckoutContext.Provider>
  )
}

export const useCheckout = () =>
  useContext<CheckoutContextType>(CheckoutContext)
