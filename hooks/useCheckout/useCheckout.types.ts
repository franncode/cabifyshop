import { Discount } from "../../features/checkout/checkout"
import { IItemCart } from "../../interfaces"

export type CheckoutContextType = {
  addItem: (code: IItemCart["code"], quantity?: number) => void
  discounts: Discount["applied"]
  items: IItemCart[]
  removeItem: (code: IItemCart["code"]) => void
  total: {
    gross: number
    net: number
  }
}
