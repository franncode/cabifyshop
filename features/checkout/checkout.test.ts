import availableItems from "../../api/items/items.json"
import pricingRules from "../../api/pricingRules/pricingRules.json"
import serverCart from "../../api/cart/cart.json"
import { IItemCart } from "../../interfaces"
import { Checkout } from "./checkout"

describe("Feature: Checkout", () => {
  const [stockShirt, stockMug, stockCap] = availableItems

  const cartItems = (checkout: Checkout) =>
    [
      checkout.getItem(stockShirt.code),
      checkout.getItem(stockMug.code),
      checkout.getItem(stockCap.code),
    ] as IItemCart[]

  test("Add items to cart", () => {
    const checkout = new Checkout(availableItems)

    checkout
      .scan(stockShirt.code)
      .scan(stockShirt.code)
      .scan(stockShirt.code)
      .scan(stockMug.code)
      .scan(stockMug.code)
      .scan(stockMug.code)
      .scan(stockMug.code)
      .scan(stockCap.code)
      .scan(stockCap.code)
      .scan(stockCap.code)
      .scan(stockCap.code)
    const [checkoutShirt, checkoutMug, checkoutCap] = cartItems(checkout)

    expect(checkoutShirt.quantity).toEqual(3)
    expect(checkoutMug.quantity).toEqual(4)
    expect(checkoutCap.quantity).toEqual(4)
  })

  test("Remove items from cart", () => {
    const checkout = new Checkout(availableItems, pricingRules, serverCart)
    const [checkoutShirt, checkoutMug, checkoutCap] = cartItems(checkout)

    checkout
      .unscan(stockShirt.code)
      .unscan(checkoutMug.code)
      .unscan(checkoutMug.code)
      .unscan(checkoutMug.code)
      .unscan(checkoutCap.code)
      .unscan(checkoutCap.code)

    expect(checkoutShirt.quantity).toEqual(2)
    expect(checkoutMug.quantity).toEqual(1)
    expect(checkoutCap.quantity).toEqual(2)
  })

  test("Get the value of all cart products without the discounts applied", () => {
    const checkout = new Checkout(availableItems, pricingRules, serverCart)
    const [checkoutShirt, checkoutMug, checkoutCap] = cartItems(checkout)

    expect(checkout.totalWithoutDiscounts()).toBe(
      checkoutShirt.total + checkoutMug.total + checkoutCap.total
    )
  })

  test("Get the value of all cart products with the discounts applied", () => {
    const checkout = new Checkout(availableItems, pricingRules, serverCart)
    const [checkoutShirt, checkoutMug, checkoutCap] = cartItems(checkout)
    checkoutShirt

    const discountShirt =
      checkoutShirt.quantity >= 3
        ? checkoutShirt.quantity * checkoutShirt.price * 0.05
        : 0
    const netShirt = checkoutShirt.total - discountShirt

    const discountMug =
      checkoutMug.quantity >= 2
        ? Math.trunc(checkoutMug.quantity / 2) * checkoutMug.price
        : 0
    const netMug = checkoutMug.total - discountMug

    expect(checkout.total()).toBe(netShirt + netMug + checkoutCap.total)
  })

  test("Get applied discounts", () => {
    const checkout = new Checkout(availableItems, pricingRules)
    checkout
      .scan(stockShirt.code)
      .scan(stockShirt.code)
      .scan(stockShirt.code)
      .scan(stockMug.code)
      .scan(stockMug.code)

    const discounts = [
      {
        code: "AS1DJN",
        item: { code: "X2G2OPZ" },
        name: "2x1 Mug offer",
        value: 5,
      },
      {
        code: "OIJF7AS",
        item: { code: "X7R2OPX" },
        name: "x3 Shirt offer",
        value: 3,
      },
    ]

    expect(checkout.getDiscounts()).toEqual(discounts)
  })
})
