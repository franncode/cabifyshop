import { IDiscount, IItem, IItemCart, IPricingRule } from "../../interfaces"

class Cart {
  public items: IItemCart[]

  constructor(items: IItemCart[] = []) {
    this.items = items
  }

  get total() {
    return this.items.reduce(
      (accumulatedTotal, { quantity, price }) =>
        accumulatedTotal + quantity * price,
      0
    )
  }

  public addItem(item: IItem) {
    const cartItem = this.getItem(item.code)
    if (cartItem) {
      cartItem.quantity += 1
      cartItem.total = cartItem.quantity * cartItem.price
    } else
      this.items.push({
        ...item,
        quantity: 1,
        total: item.price,
      })
  }

  public changeItem(item: IItem, quantity: IItemCart["quantity"] = 0) {
    const cartItem = this.getItem(item.code)
    if (cartItem) {
      cartItem.quantity = quantity
      cartItem.total = cartItem.quantity * cartItem.price
    } else
      this.items.push({
        ...item,
        quantity: quantity,
        total: item.price,
      })
  }

  public removeItem(
    code: IItemCart["code"],
    quantity: IItemCart["quantity"] = 1
  ) {
    const cartItem = this.getItem(code)

    if (!cartItem) throw new Error(`Item ${code} doesn't exist in cart`)
    if (cartItem.quantity === 1) {
      this.items = this.items.filter((item) => item.code !== code)
    } else {
      cartItem.quantity = cartItem.quantity - quantity
      cartItem.total = cartItem.quantity * cartItem.price
    }
  }

  public getItem(code: IItemCart["code"]): IItemCart | undefined {
    return this.items.find((item) => item.code === code)
  }
}

export class Discount {
  private readonly pricingRules: IPricingRule[]
  public applied: IDiscount[] = []

  constructor(pricingRules: IPricingRule[] = []) {
    this.pricingRules = pricingRules
  }

  get total() {
    return this.applied.reduce(
      (accumulatedTotal, { value }) => accumulatedTotal + value,
      0
    )
  }

  public apply(items: IItemCart[]) {
    let applied: IDiscount[] = []
    this.pricingRules.forEach(({ apply, code, name, rule, item }) => {
      const compliesWithRule = new Function("item", `return ${rule}`)
      const applyDiscount = new Function("item", `return ${apply}`)
      const itemToApply = items.find(({ code }) => code === item.code)

      if (itemToApply && compliesWithRule(itemToApply)) {
        applied.push({
          code,
          item: { code: itemToApply.code },
          name,
          value: applyDiscount(itemToApply),
        })
      }
    })
    this.applied = applied
  }
}

export class Checkout {
  private cart: Cart
  private discounts: Discount
  private readonly availableItems: IItem[]

  constructor(
    availableItems: IItem[] = [],
    pricingRules: IPricingRule[] = [],
    cartItems: IItemCart[] = []
  ) {
    this.availableItems = availableItems
    this.cart = new Cart(cartItems)
    this.discounts = new Discount(pricingRules)
    if (this.getItems().length > 0) this.discounts.apply(this.cart.items)
  }

  /**
   * Scans a product adding it to the current cart.
   * @param code The product identifier
   * @returns itself to allow function chaining
   */
  public scan(code: IItemCart["code"], quantity?: IItemCart["quantity"]): this {
    const item = this.availableItems.find(
      (availableItem) => availableItem.code === code
    )
    if (item) {
      if (typeof quantity === "number") this.cart.changeItem(item, quantity)
      else this.cart.addItem(item)
      this.discounts.apply(this.cart.items)
    } else throw new Error(`Item ${code} not found`)
    return this
  }

  /**
   * Scans a product remove it from the current cart.
   * @param code The product identifier
   * @returns itself to allow function chaining
   */
  public unscan(code: IItemCart["code"]): this {
    this.cart.removeItem(code)
    this.discounts.apply(this.cart.items)
    return this
  }

  /**
   * Returns the value of all cart products with the discounts applied.
   */
  public total(): number {
    return this.cart.total - this.discounts.total
  }

  /**
   * Returns the value of all cart products without discounts applied.
   */
  public totalWithoutDiscounts(): number {
    return this.cart.total
  }

  /**
   * Returns the cart products.
   */
  public getItems(): IItemCart[] {
    return this.cart.items
  }

  public getItem(code: IItemCart["code"]): IItemCart | undefined {
    return this.cart.getItem(code)
  }

  public getDiscounts(): IDiscount[] {
    return this.discounts.applied
  }
}
