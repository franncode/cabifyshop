export const { format } = Intl.NumberFormat("es-ES", {
  currency: "EUR",
  minimumFractionDigits: 0,
  style: "currency",
})

export const currency = {
  toEUR: format,
}
