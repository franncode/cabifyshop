import availableItems from "../../api/items/items.json"
import { currency } from "./currency"

describe("Feature: Currency", () => {
  test("Number to EUR", () => {
    const number = 123.45
    const price = currency.toEUR(number)

    expect(price).toBe("123,45 €")
  })
})
