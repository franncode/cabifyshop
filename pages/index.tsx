import { NextPage } from "next"
import { useState } from "react"
import { CartDetails } from "../components/templates/cartDetails/cartDetails"
import { ItemDetails } from "../components/templates/itemDetails/itemDetails"
import { IItemCart } from "../interfaces"

const CheckoutPage: NextPage = () => {
  const [itemPreview, setItemPreview] = useState<IItemCart | undefined>(
    undefined
  )

  return itemPreview ? (
    <ItemDetails item={itemPreview} onClose={() => setItemPreview(undefined)} />
  ) : (
    <CartDetails onPreview={(item) => setItemPreview(item)} />
  )
}

export default CheckoutPage
