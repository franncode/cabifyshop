import { Provider as RollbarProvider } from "@rollbar/react"
import type { AppProps } from "next/app"
import { CheckoutProvider } from "../hooks/useCheckout/useCheckout"
import { rollbarConfig } from "../settings/rollbar"
import "../styles/globals.scss"

const CabifyShop = ({ Component, pageProps }: AppProps) => {
  return (
    <CheckoutProvider>
      <RollbarProvider config={rollbarConfig}>
        <Component {...pageProps} />
      </RollbarProvider>
    </CheckoutProvider>
  )
}

export default CabifyShop
